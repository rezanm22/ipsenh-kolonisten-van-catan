import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JsonHeaderInterceptor } from './json-header-interceptor';
import { AuthInterceptor } from './auth-interceptor';
import { AuthErrorInterceptor } from './auth-error-interceptor';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: JsonHeaderInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthErrorInterceptor, multi: true }
];
