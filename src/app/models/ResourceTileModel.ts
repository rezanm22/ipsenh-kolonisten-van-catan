import {TileModel} from './TileModel';
import { ResourceNames } from './ResourceNames';
import { CardTypes } from './CardTypes';

export class ResourceTileModel extends TileModel {

  constructor(public resourceName: ResourceNames, public value: number, public diceNumber: number) {
    super('/assets/images/' + resourceName + diceNumber + '.png', CardTypes.RESOURCE);
  }

  getPath() {
    return '/assets/images/' + this.resourceName + this.diceNumber;
  }

}
