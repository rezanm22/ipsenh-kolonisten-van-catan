import { GameBoardModel } from './GameBoardModel';
import { DiceModel } from './DiceModel';

export class DiceAndBoardModel {
    diceResult?: DiceModel;
    gameBoard?: GameBoardModel;
}
