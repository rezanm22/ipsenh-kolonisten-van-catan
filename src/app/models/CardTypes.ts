export enum CardTypes {
  STREET = 'straat',
  RESOURCE = 'grondstof',
  VILLAGE = 'dorp',
  BUILD = 'bouw'
}
