import {TileModel} from './TileModel';
import {CardTypes} from './CardTypes';

export class BuildTileModel extends TileModel {

  constructor() {
    super('/assets/images/versterkt_pakhuis.png', CardTypes.BUILD);
  }
}
