import { ResourceTileModel } from './ResourceTileModel';
import { Injectable } from '@angular/core';

export enum ResourceNames {
  GRAIN = 'graan',
  WOOD = 'hout',
  ORE = 'ore',
  SHEEP = 'schaap',
  STONE = 'steen',
  GOLD = 'goud'
}

@Injectable({
  providedIn: 'root'
})
export class RandomResourceName {
  public get() {
    return this.randomEnum(ResourceNames);
  }

  public getTile() {
    return new ResourceTileModel(this.get(), 1, 1);
  }

  randomEnum<T>(anEnum: T): T[keyof T] {
    const enumValues = (Object.values(anEnum) as unknown) as T[keyof T][];
    const randomIndex = Math.floor(Math.random() * enumValues.length);
    return enumValues[randomIndex];
  }
}
