import {TileModel} from './TileModel';
import {VillageTileModel} from './VillageTileModel';
import { CardTypes } from './CardTypes';

export class StreetTileModel extends TileModel {

  left: VillageTileModel;
  right: VillageTileModel;

  constructor() {
    super('/assets/images/middenstraat.png', CardTypes.STREET);
  }
}
