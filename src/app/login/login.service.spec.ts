import { TestBed } from '@angular/core/testing';

import { LoginService } from './login.service';
import { HttpClientModule } from '@angular/common/http';
import { UserModel } from '../models/UserModel';

describe('LoginService', () => {

  let service: LoginService;

  function getUserModel() {
    return new UserModel(1, 'useruser', 'passpass');
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.get(LoginService);
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have a function for registering', () => {
    const value = service.register(getUserModel());
    expect(value).toBeTruthy();
  });

  it('should have a function for login', () => {
    const value = service.login(getUserModel());
    expect(value).toBeTruthy();
  });
});
