import {Component, OnInit} from '@angular/core';
import {Validators, FormBuilder} from '@angular/forms';
import {LoginService} from '../login.service';
import {AuthService} from 'src/app/shared/àuth/auth.service';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  errorMessage: string;
  readonly succesFullRegister: string = 'Account succesvol aangemaakt!';

  registerForm = this.formBuilder.group({
    username: ['', [Validators.required, Validators.minLength(4)]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  });

  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private router: Router,
              private toastr: ToastrService, private authService: AuthService) {
  }

  ngOnInit() {
    this.setBackgroundColor();
    this.authService.removeToken();
  }

  onSubmit() {
    this.loginService.register(this.registerForm.value).subscribe(
      () => {
        this.router.navigate(['/login']);
        this.toastr.success(this.succesFullRegister);
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }

  isPasswordInvalid(): boolean {
    return this.registerForm.controls.password.invalid;
  }

  isUsernameInvalid(): boolean {
    return this.registerForm.controls.username.invalid;
  }

  getPasswordErrorMessage(): string {
    return 'Wachtwoord moet minimaal 8 tekens lang zijn';
  }

  getUsernameErrorMessage(): string {
    return 'Username moet minimaal 4 tekens lang zijn';
  }

  setBackgroundColor() {
    document.body.style.backgroundColor = '#D32E00';
  }
}
