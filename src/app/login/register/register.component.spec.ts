import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from '../login-routing.module';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from '../login/login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule } from 'ngx-toastr';
import { throwError, of } from 'rxjs';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  const loginServiceSpy = jasmine.createSpyObj('loginService', ['register']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterComponent, LoginComponent ],
      imports: [
        CommonModule,
        LoginRoutingModule,
        MatInputModule,
        MatCardModule,
        ReactiveFormsModule,
        MatButtonModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
        ToastrModule.forRoot()
      ],
      providers: [
        { provide: LoginService, useValue: loginServiceSpy }
      ]
    })
    .compileComponents();
    loginServiceSpy.register.and.returnValue(of('somevalue'));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should call loginService.register on form submitting', () => {
    component.onSubmit();

    expect(loginServiceSpy.register).toHaveBeenCalled();
  });


  const testerror = {
    error: {
      message: 'error'
    }
  };

  it('should show an error on an insuccesfull register', () => {
    loginServiceSpy.register.and.returnValue(throwError(testerror));

    component.onSubmit();

    expect(component.errorMessage).toBeDefined();
  });

  it('isPasswordInvalid should return true on a invalid password as aaa', () => {
    const expectedValue = true;

    component.registerForm.controls.password.setValue('aaa');
    const actualValue = component.isPasswordInvalid();

    expect(actualValue).toBe(expectedValue);
  });

  it('isPasswordInvalid should return false on a valid password as aaaaaaaa', () => {
    const expectedValue = false;

    component.registerForm.controls.password.setValue('aaaaaaaa');
    const actualValue = component.isPasswordInvalid();

    expect(actualValue).toBe(expectedValue);
  });

  it('isUsernameInvalid should return true on a invalid username as aaa', () => {
    const expectedValue = true;

    component.registerForm.controls.username.setValue('aaa');
    const actualValue = component.isUsernameInvalid();

    expect(actualValue).toBe(expectedValue);
  });

  it('isUsernameInvalid should return false on a valid username as aaaaaaaa', () => {
    const expectedValue = false;

    component.registerForm.controls.username.setValue('aaaaaaaa');
    const actualValue = component.isUsernameInvalid();

    expect(actualValue).toBe(expectedValue);
  });

});
