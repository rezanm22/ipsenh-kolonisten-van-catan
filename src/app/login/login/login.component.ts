import {Component, OnInit} from '@angular/core';
import {Validators, FormBuilder} from '@angular/forms';
import {LoginService} from '../login.service';
import {AuthService} from 'src/app/shared/àuth/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {TokenModel} from 'src/app/models/TokenModel';
import * as moment from 'moment';
import {HeaderService} from '../../header/header.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public errorMessage: string;
  readonly incorrectAuthMessage: string = 'incorrecte inloggevens';
  readonly succesFullLogin: string = 'Succesvol ingelogd!';

  public loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(private formBuilder: FormBuilder, private loginService: LoginService,
              private authService: AuthService, private router: Router,
              private toastr: ToastrService, private headerService: HeaderService) {
  }

  ngOnInit() {
    this.authService.removeToken();
    this.setBackgroundColor();
  }

  onSubmit() {
    this.loginService.login(this.loginForm.value).subscribe(
      (val: TokenModel) => {
        this.login(val);
      },
      () => {
        this.errorMessage = this.incorrectAuthMessage;
      }
    );
  }

  private login(tokenModel: TokenModel) {
    this.headerService.saveUsername(this.loginForm.value.username);
    this.authService.saveToken(tokenModel);
    this.router.navigate(['/home']);
    this.toastr.success(this.succesFullLogin);
  }

  setBackgroundColor() {
    document.body.style.backgroundColor = '#D32E00';
  }
}
