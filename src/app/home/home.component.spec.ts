import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HomeComponent} from './home.component';
import {CommonModule} from '@angular/common';
import {HeaderModule} from '../header/header.module';
import {HomeRoutingModule} from './home-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        CommonModule,
        HeaderModule,
        HomeRoutingModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(
          []
        ),
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Hide ScrollBar', () => {
    component.removeScrollBar();
    expect(document.body.style.overflow).toEqual('hidden');
  });
});
