import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LobbyService {

  constructor(private http: HttpClient) {
  }


  public startGame(): Observable<any> {
    const body = {'': ''};
    const url = environment.api_url + '/api/startGame';
    return this.http.post(url, body);
  }

  public getUsersInLobby(): Observable<any> {
    const url = environment.api_url + '/api/usersFromCurrentLobby';
    return this.http.get(url);
  }

  public getLobbyForUser(): Observable<any> {
    const url = environment.api_url + '/api/lobbyForCurrentUser';
    return this.http.get(url);
  }

}
