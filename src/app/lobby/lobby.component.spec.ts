import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LobbyComponent} from './lobby.component';
import {CommonModule} from '@angular/common';
import {MatCardModule} from '@angular/material';
import {HeaderModule} from '../header/header.module';
import {LobbyRoutingModule} from './lobby-routing.module';
import {ChatModule} from '../chat/chat.module';
import {LobbyService} from './lobby.service';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('LobbyComponent', () => {
  let component: LobbyComponent;
  let fixture: ComponentFixture<LobbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LobbyComponent],
      imports: [
        CommonModule,
        MatCardModule,
        HeaderModule,
        LobbyRoutingModule,
        ChatModule,
        HeaderModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(
          []
        )],
      providers: [LobbyService],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // // it('should create', () => {
  // //   expect(component).toBeTruthy();
  // });
});
