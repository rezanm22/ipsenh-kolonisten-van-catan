import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {AddFriendComponent} from './add-friend/add-friend.component';
import {FriendsService} from './friends.service';
import {UserModel} from '../models/UserModel';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  public userFriendList: UserModel[] = [];
  pageNumber: number;
  searchTerm: string;

  constructor(private dialog: MatDialog, private friendService: FriendsService, private toastr: ToastrService,
              private router: Router, private route: ActivatedRoute) {
    this.fillUserFriendList();
  }

  ngOnInit() {
    this.setBackgroundColor();
    this.friendService.result.on('', () => {
      this.fillUserFriendList();
    });
  }

  fillUserFriendList() {
    this.friendService.isUserChallenged().subscribe(
      val => {
        this.router.navigate(['lobby']);
      }
    );
    this.friendService.getUserFriends().subscribe(
      val => {
        this.userFriendList = val;
      }
    );
  }

  setBackgroundColor() {
    document.body.style.backgroundColor = '#D32E00';
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    this.dialog.open(AddFriendComponent, dialogConfig);
  }

  challengeFriend(username: string) {
    this.friendService.challengeFriend(username).subscribe(
      val => {
        this.toastr.success('Lobby succesvol aangemaakt!');
        this.router.navigate(['lobby']);
      },
      err => {
        console.log('something went wrong', err);
        this.toastr.error('Lobby mislukt!');
      }
    );
  }
}
