import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {EventEmitter} from 'events';

@Injectable({
  providedIn: 'root'
})
export class FriendsService {
  result: EventEmitter = new EventEmitter();

  constructor(private http: HttpClient) {
  }

  public isUserChallenged(): Observable<any> {
    const url = environment.api_url + '/api/isCurrentUserChallenged';
    return this.http.get(url);
  }


  public addFriend(usernameFriend): Observable<any> {
    const body = {friend: usernameFriend};
    console.log(body);
    const url = environment.api_url + '/api/addFriend';
    return this.http.post(url, body);
  }

  public getUserFriends(): Observable<any> {
    const url = environment.api_url + '/api/friendsFromCurrentUser';
    return this.http.get(url);
  }

  public challengeFriend(usernameFriend): Observable<any> {
    const body = {friend: usernameFriend};
    console.log(body);
    const url = environment.api_url + '/api/challengeFriend';
    return this.http.post(url, body);
  }

}
