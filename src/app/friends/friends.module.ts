import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FriendsComponent} from './friends.component';
import {HeaderModule} from '../header/header.module';
import {MatCardModule, MatDialogModule, MatFormFieldModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {AddFriendComponent} from './add-friend/add-friend.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FriendsRoutingModule} from './friends-routing.module';
import {FriendsService} from './friends.service';
import {ToastrService} from 'ngx-toastr';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [FriendsComponent, AddFriendComponent],
  exports: [
    FriendsComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    MatCardModule,
    NgxPaginationModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    FormsModule,
    FriendsRoutingModule,
    SharedModule
  ],
  providers: [FriendsService, ToastrService],
  entryComponents: [AddFriendComponent]
})
export class FriendsModule {
}
