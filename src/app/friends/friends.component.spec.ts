import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FriendsComponent} from './friends.component';
import {CommonModule} from '@angular/common';
import {HeaderModule} from '../header/header.module';
import {MatCardModule, MatDialogModule, MatFormFieldModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FriendsRoutingModule} from './friends-routing.module';
import {SharedModule} from '../shared/shared.module';
import {FriendsService} from './friends.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('FriendsComponent', () => {
  let component: FriendsComponent;
  let fixture: ComponentFixture<FriendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FriendsComponent],
      imports: [
        CommonModule,
        HeaderModule,
        MatCardModule,
        NgxPaginationModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        FormsModule,
        FriendsRoutingModule,
        SharedModule,
        HttpClientModule,
        ToastrModule.forRoot(),
        RouterTestingModule.withRoutes(
          []
        ),
      ],
      providers: [FriendsService, ToastrService],
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(FriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('backGroundColor Red', () => {
    component.setBackgroundColor();
    expect(document.body.style.backgroundColor).toEqual('rgb(211, 46, 0)');
  });
});
