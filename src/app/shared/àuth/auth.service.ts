import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TokenModel } from 'src/app/models/TokenModel';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly ACCESSTOKENKEY = 'access_token';
  private readonly REFRESHTOKENKEY = 'refresh_token';
  private readonly EXPIREDATEKEY = 'expire_date_token';

  constructor(private http: HttpClient, private router: Router) {
  }

  saveToken(token: TokenModel) {
    this.saveAccessTokenAndExpiredate(token.access);
    this.saveRefreshToken(token.refresh);
  }

  saveAccessTokenAndExpiredate(accessToken: string): void {
    const expireDate = this.getExpireDate(accessToken);

    localStorage.setItem(this.ACCESSTOKENKEY, accessToken);
    localStorage.setItem(this.EXPIREDATEKEY, expireDate);
  }

  saveRefreshToken(refreshToken: string): void {
    localStorage.setItem(this.REFRESHTOKENKEY, refreshToken);
  }

  private getExpireDate(accessToken: string): string {
    const tokenParts = accessToken.split(/\./);
    const parsedData = JSON.parse(window.atob(tokenParts[1]));
    return parsedData.exp;
  }

  getAccessToken(): string {
    return localStorage.getItem(this.ACCESSTOKENKEY);
  }

  isTokenExpired(): boolean {
    const currentTimeUTC = moment().utc().valueOf();
    const expireDate = localStorage.getItem(this.EXPIREDATEKEY);
    return currentTimeUTC > parseInt(expireDate, 0) * 1000;
  }

  isLoggedIn(): boolean {
    return this.getAccessToken() !== null;
  }

  removeToken(): void {
    localStorage.removeItem(this.ACCESSTOKENKEY);
    localStorage.removeItem(this.REFRESHTOKENKEY);
    localStorage.removeItem(this.EXPIREDATEKEY);
  }

  logout(): void {
    this.removeToken();
    this.router.navigate(['login']);
  }

  requestNewToken(): Observable<any> {
    const url = environment.api_url + '/api/token/refresh/';
    const json = JSON.stringify({refresh: localStorage.getItem(this.REFRESHTOKENKEY)});
    return this.http.post(url, json);
  }
}
