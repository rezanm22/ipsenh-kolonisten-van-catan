import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(private http: HttpClient) {

  }

  saveUsername(username: string): void {
    localStorage.setItem('username', username);
  }
  getUsernameInLocalStorage(): string {
    return localStorage.getItem('username');
  }

}
