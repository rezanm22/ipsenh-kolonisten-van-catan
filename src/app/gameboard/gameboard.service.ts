import { Injectable } from '@angular/core';
import { DiceAndBoardModel } from '../models/DiceAndBoardModel';
import { GameBoardModel } from '../models/GameBoardModel';
import { DiceModel } from '../models/DiceModel';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GameboardService {

  constructor(private http: HttpClient) { }

  public updateBoardTiles(gameBoard: GameBoardModel, diceResult: DiceModel) {
    const diceAndBoardModel = this.createDiceAndBoardObject(gameBoard, diceResult);
    return this.getRequestToUpdateBoard(diceAndBoardModel);
  }

  private createDiceAndBoardObject(gameBoard: GameBoardModel, diceResult: DiceModel): DiceAndBoardModel {
    const TempDiceAndBoard = new DiceAndBoardModel();
    TempDiceAndBoard.gameBoard = gameBoard;
    TempDiceAndBoard.diceResult = diceResult;
    return TempDiceAndBoard;
  }

  private getRequestToUpdateBoard(diceAndBoardModel: DiceAndBoardModel) {
    const url = environment.api_url + '/api/updateGameboard';
    return this.http.post(url, diceAndBoardModel);
  }

  public procesError(err: Error) {
    console.log(err);
  }
}
