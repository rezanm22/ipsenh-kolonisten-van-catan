import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameboardimageComponent } from './gameboardimage.component';

describe('GameboardimageComponent', () => {
  let component: GameboardimageComponent;
  let fixture: ComponentFixture<GameboardimageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameboardimageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameboardimageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
