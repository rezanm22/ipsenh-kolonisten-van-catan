import {Component, Input, OnInit} from '@angular/core';
import {TileModel} from 'src/app/models/TileModel';
import {EventEmitter} from 'events';
import {VillageTileModel} from '../../models/VillageTileModel';
import {BuildTileModel} from '../../models/BuildTileModel';
import {StreetTileModel} from '../../models/StreetTileModel';
import {ResourceTileModel} from '../../models/ResourceTileModel';
import {RandomResourceName} from '../../models/ResourceNames';
import {CardTypes} from '../../models/CardTypes';

@Component({
  selector: 'app-gameboardimage',
  templateUrl: './gameboardimage.component.html',
  styleUrls: ['./gameboardimage.component.css']
})
export class GameboardimageComponent implements OnInit {

  @Input() centerTile: TileModel;
  @Input() marginLeft: number;
  selectedCard: TileModel;
  @Input() newTileEvent: EventEmitter;

  constructor(private randomResourceName: RandomResourceName) {
  }


  ngOnInit() {
    this.newTileEvent.on('', (val) => {
      this.selectedCard = val;
    });
  }

  topTileClicked() {
    if (this.centerTileIsVillage()) {
      if (this.selectedCardIsBuildCard()) {
        this.placeTopTile(this.centerTile as VillageTileModel);
      }
    }
  }

  bottomTileClicked() {
    if (this.centerTileIsVillage()) {
      if (this.selectedCardIsBuildCard()) {
        this.placeBottomTile(this.centerTile as VillageTileModel);
      }
    }
  }

  leftTileClicked() {
    if (this.centerTileIsVillage()) {
      if (this.selectedCardIsStreet()) {
        this.placeStreetOnLeftTile(this.centerTile as VillageTileModel);
      }
    }
    if (this.centerTileIsStreet()) {
      if (this.selectedCardIsVillage()) {
        this.placeVillageOnLeftTile(this.centerTile as StreetTileModel, this.selectedCard as VillageTileModel);
      }
    }
  }

  rightTileClicked() {
    if (this.centerTileIsVillage()) {
      if (this.selectedCardIsStreet()) {
        this.placeStreetOnRightTile(this.centerTile as VillageTileModel);
      }
    }
    if (this.centerTileIsStreet()) {
      if (this.selectedCardIsVillage()) {
        this.placeVillageOnRightTile(this.centerTile as StreetTileModel, this.selectedCard as VillageTileModel);
      }
    }
  }

  setResourceCardRotationByValue(value): string {
    switch (value) {
      case 0:
        return 'rotate(90deg)';
        break;
      case 1:
        return 'rotate(0deg)';
        break;
      case 2:
        return 'rotate(270deg)';
        break;
      case 3:
        return 'rotate(180deg)';
        break;
      default:
        return 'rotate(180deg)';
        break;
    }
  }

  centerTileIsVillage() {
    return this.centerTile.type === CardTypes.VILLAGE;
  }

  centerTileIsStreet() {
    return this.centerTile.type === CardTypes.STREET;
  }

  selectedCardIsBuildCard() {
    return this.selectedCard.type === CardTypes.BUILD;
  }

  selectedCardIsStreet() {
    return this.selectedCard.type === CardTypes.STREET;
  }

  selectedCardIsVillage() {
    return this.selectedCard.type === CardTypes.VILLAGE;
  }

  placeTopTile(centerTile: VillageTileModel) {
    centerTile.top = this.selectedCard as BuildTileModel;
    this.newTileEvent.emit('placed', this.selectedCard);
  }

  placeBottomTile(centerTile: VillageTileModel) {
    centerTile.bottom = this.selectedCard as BuildTileModel;
    this.newTileEvent.emit('placed', this.selectedCard);
  }

  placeStreetOnLeftTile(centerTile: VillageTileModel) {
    centerTile.left = this.selectedCard as StreetTileModel;
    this.newTileEvent.emit('placed', this.selectedCard);
  }

  placeStreetOnRightTile(centerTile: VillageTileModel) {
    centerTile.right = this.selectedCard as StreetTileModel;
    this.newTileEvent.emit('placed', this.selectedCard);
  }

  placeVillageOnLeftTile(centerTile: StreetTileModel, selectedCard: VillageTileModel) {
    centerTile.left = selectedCard;
    selectedCard.topleft = new ResourceTileModel(this.getRandomResourceName(), 1, 1);
    selectedCard.bottomleft = new ResourceTileModel(this.getRandomResourceName(), 1, 1);
    this.newTileEvent.emit('placed', this.selectedCard);
  }

  placeVillageOnRightTile(centerTile: StreetTileModel, selectedCard: VillageTileModel) {
    centerTile.right = selectedCard;
    selectedCard.topright = new ResourceTileModel(this.getRandomResourceName(), 1, 1);
    selectedCard.bottomright = new ResourceTileModel(this.getRandomResourceName(), 1, 1);
    this.newTileEvent.emit('placed', this.selectedCard);
  }

  getRandomResourceName() {
    return this.randomResourceName.get();
  }
}
