import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GameboardComponent} from './gameboard.component';
import {GameboardRoutingModule} from './gameboard-routing.module';
import {HeaderModule} from '../header/header.module';
import {DiceComponent} from './dice/dice.component';
import {GameboardimageComponent} from './gameboardimage/gameboardimage.component';
import {RandomResourceName} from '../models/ResourceNames';


@NgModule({
  declarations: [GameboardComponent, DiceComponent, GameboardimageComponent],
  imports: [
    CommonModule,
    GameboardRoutingModule,
    HeaderModule,
  ],
  providers: [RandomResourceName]
})
export class GameboardModule {
}
