import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {GameboardComponent} from './gameboard.component';

const routes: Routes = [
  {path: 'game', component: GameboardComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameboardRoutingModule {
}
