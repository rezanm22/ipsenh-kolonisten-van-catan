import { Component, OnInit } from '@angular/core';
import { DiceService } from './dice.service';
import { DiceModel } from 'src/app/models/DiceModel';

@Component({
  selector: 'app-dice',
  templateUrl: './dice.component.html',
  styleUrls: ['./dice.component.css']
})
export class DiceComponent implements OnInit {

  diceResult = 6;
  readonly dieListOddRollCss = 'die-list even-roll';
  readonly dieListEvenRollCss = 'die-list odd-roll';
  cssSelectorDice = this.dieListEvenRollCss;

  constructor(private diceService: DiceService) { }

  ngOnInit() {
    this.subscribeToRollDiceEvent();
  }

  private subscribeToRollDiceEvent() {
    this.diceService.getDiceEventEmitter().on('roll', () => {
      this.rollDice();
    });
  }

  rollDice() {
    this.animateDiceRoll();
    this.emitDiceResultFromApi();
  }

  private animateDiceRoll() {
    if (this.cssSelectorDice === this.dieListEvenRollCss) {
      this.cssSelectorDice = this.dieListOddRollCss;
    } else {
      this.cssSelectorDice = this.dieListEvenRollCss;
    }
  }

  private emitDiceResultFromApi() {
    this.diceService.requestDiceRollApi().subscribe(
      (val: DiceModel) => {
        this.diceService.emitDiceResult(val);
        this.diceResult = val.diceResult;
      },
      err => {
        // impement error handeling
      }
    );
  }

}
