import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScoreboardComponent} from './scoreboard.component';
import {CommonModule} from '@angular/common';
import {HeaderModule} from '../header/header.module';
import {HomeRoutingModule} from '../home/home-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {MatCardModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {ScoreboardRoutingModule} from './scoreboard-routing.module';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';

describe('ScoreboardComponent', () => {
  let component: ScoreboardComponent;
  let fixture: ComponentFixture<ScoreboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScoreboardComponent],
      imports: [
        CommonModule,
        HeaderModule,
        MatCardModule,
        NgxPaginationModule,
        ScoreboardRoutingModule,
        SharedModule,
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(
          []
        ),
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('backGroundColor Red', () => {
    component.setBackgroundColor();
    expect(document.body.style.backgroundColor).toEqual('rgb(211, 46, 0)');
  });
});
