import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserModel} from '../models/UserModel';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScoreboardService {

  constructor(private http: HttpClient) {
  }

  public getAllUsers(): Observable<any> {
    const url = environment.api_url + '/api/users';
    return this.http.get(url);
  }
}

