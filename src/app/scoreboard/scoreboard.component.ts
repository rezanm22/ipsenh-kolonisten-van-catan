import { Component, OnInit } from '@angular/core';
import {UserModel} from '../models/UserModel';
import {ScoreboardService} from './scoreboard.service';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {
  pageNumber: number;
  public userList: UserModel[] = [];
  searchTerm: string;

  constructor(private scoreBoardService: ScoreboardService) {
    scoreBoardService.getAllUsers().subscribe(
      val => {
        this.userList = val;
      }
    );
  }

  ngOnInit() {
    this.setBackgroundColor();
  }



  setBackgroundColor() {
    document.body.style.backgroundColor = '#D32E00';
  }

}
